#!/usr/bin/env python3

def warn(*args, **kwargs):
    pass
    
import os, warnings
warnings.warn = warn

from ecradplot import plot as eplt

def main(input_srcfile, output_srcfile, reference_srcfile, reference_label, dstdir, title):
    """
    Plot input files
    """
    
    import os
    if not os.path.isdir(dstdir):
        os.makedirs(dstdir)

    name_string = os.path.splitext(os.path.basename(output_srcfile))[0]
    
    if not title:
        title = name_string
    
    if reference_srcfile:
        dstfile_LW_fluxes = os.path.join(dstdir, name_string + f"_vs_{reference_label}_LW_fluxes.png")
        dstfile_SW_fluxes = os.path.join(dstdir, name_string + f"_vs_{reference_label}_SW_fluxes.png")
        dstfile_CRE = os.path.join(dstdir, name_string + f"_vs_{reference_label}_CRE.png")
        dstfile_heating_rates_linp = os.path.join(dstdir, name_string + f"_vs_{reference_label}_heating_rates_linp.png")
        dstfile_heating_rates_logp = os.path.join(dstdir, name_string + f"_vs_{reference_label}_heating_rates_logp.png") 

        print(f"Plotting LW fluxes to {dstfile_LW_fluxes}")
        eplt.plot_LW_flux_difference(input_srcfile, output_srcfile, reference_srcfile, dstfile=dstfile_LW_fluxes, title=title);
        
        print(f"Plotting SW fluxes to {dstfile_SW_fluxes}")
        eplt.plot_SW_flux_difference(input_srcfile, output_srcfile, reference_srcfile, dstfile=dstfile_SW_fluxes, title=title);
        
        print(f"Plotting cloud radiative effects to {dstfile_CRE}")
        eplt.plot_CRE_difference(input_srcfile, output_srcfile, reference_srcfile, dstfile=dstfile_CRE, title=title);
        
        print(f"Plotting tropospheric heating rates to {dstfile_heating_rates_linp}")
        eplt.plot_heating_rate_difference(input_srcfile, output_srcfile, reference_srcfile, dstfile=dstfile_heating_rates_linp, 
                                          title=title);
        
        print(f"Plotting stratospheric heating rates to {dstfile_heating_rates_logp}")
        eplt.plot_heating_rate_difference(input_srcfile, output_srcfile, reference_srcfile, dstfile=dstfile_heating_rates_logp, 
                                          linear_pressure=False, title=title);
        
    else:
        dstfile_LW_fluxes = os.path.join(dstdir, name_string + "_LW_fluxes.png")
        dstfile_SW_fluxes = os.path.join(dstdir, name_string + "_SW_fluxes.png")
        dstfile_CRE = os.path.join(dstdir, name_string + "_CRE.png")
        dstfile_heating_rates_linp = os.path.join(dstdir, name_string + "_heating_rates_linp.png")
        dstfile_heating_rates_logp = os.path.join(dstdir, name_string + "_heating_rates_logp.png") 

        print(f"Plotting LW fluxes to {dstfile_LW_fluxes}")
        eplt.plot_LW_flux(input_srcfile, output_srcfile, dstfile=dstfile_LW_fluxes, title=title);
        
        print(f"Plotting SW fluxes to {dstfile_SW_fluxes}")
        eplt.plot_SW_flux(input_srcfile, output_srcfile, dstfile=dstfile_SW_fluxes, title=title);
        
        print(f"Plotting cloud radiative effects to {dstfile_CRE}")
        eplt.plot_CRE(input_srcfile, output_srcfile, dstfile=dstfile_CRE, title=title);
        
        print(f"Plotting tropospheric heating rates to {dstfile_heating_rates_linp}")
        eplt.plot_heating_rate(input_srcfile, output_srcfile, dstfile=dstfile_heating_rates_linp, title=title);
        
        print(f"Plotting stratospheric heating rates to {dstfile_heating_rates_logp}")
        eplt.plot_heating_rate(input_srcfile, output_srcfile, dstfile=dstfile_heating_rates_logp, linear_pressure=False, title=title);
    
if __name__ == "__main__":
    import argparse
    parser = argparse.ArgumentParser(description="Plot radiative fluxes and heating rates from ecRAD output file.")
    parser.add_argument("input",    help="ecRAD input file")
    parser.add_argument("output",   help="ecRAD output file")
    parser.add_argument("--reference", help="ecRAD output file to use as a reference", default=None)
    parser.add_argument("--reference_label", help="Label for ecRAD output file used as a reference", default=None)
    parser.add_argument("--dstdir", help="Destination directory for plots", default="./")
    parser.add_argument("--title",  help="Overwrite figure title", default=None)
    args = parser.parse_args()
    
    main(args.input, args.output, args.reference, args.reference_label, args.dstdir, args.title)
