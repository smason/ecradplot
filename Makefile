help:
	echo "Type make test";

SRCDIR = /perm/rd/pasm/ecrad_offline
DSTDIR = ./test/

#IFS = era5_2001-04-01-00_lon150
IFS = era5_2019-07-11-12_lon0
MCICA = $(IFS)_mcica
SPART = $(IFS)_spartacus
TRIPL = $(IFS)_tc

IFS_SRCFILE   = $(SRCDIR)/$(IFS).nc
MCICA_SRCFILE = $(SRCDIR)/$(MCICA).nc
SPART_SRCFILE = $(SRCDIR)/$(SPART).nc
TRIPL_SRCFILE = $(SRCDIR)/$(TRIPL).nc

LATITUDES = -55 -15 5 30

#Plot inputs
test_inputs: 
	./ecrad_plot_input.py $(IFS_SRCFILE) --dstdir=$(DSTDIR)

test_input_profile:
	./ecrad_plot_input_profile.py $(IFS_SRCFILE) --latitudes $(LATITUDES) --dstdir=$(DSTDIR)


#Plot results
test_output:
	./ecrad_plot_output.py $(IFS_SRCFILE) $(MCICA_SRCFILE) --dstdir=$(DSTDIR)
	./ecrad_plot_output.py $(IFS_SRCFILE) $(SPART_SRCFILE) --dstdir=$(DSTDIR)
	./ecrad_plot_output.py $(IFS_SRCFILE) $(TRIPL_SRCFILE) --dstdir=$(DSTDIR)

test_output_difference:
	./ecrad_plot_output.py $(IFS_SRCFILE) $(SPART_SRCFILE) --dstdir=$(DSTDIR) --reference $(MCICA_SRCFILE) --reference_label "McICA"
	./ecrad_plot_output.py $(IFS_SRCFILE) $(TRIPL_SRCFILE) --dstdir=$(DSTDIR) --reference $(MCICA_SRCFILE) --reference_label "McICA"


test_output_scalar:
	./ecrad_plot_output_scalar.py $(IFS_SRCFILE) $(MCICA_SRCFILE) $(SPART_SRCFILE) $(TRIPL_SRCFILE) --labels "McICA" "SPARTACUS" "TripleClouds" --dstdir=$(DSTDIR)

test_output_scalar_difference:
	./ecrad_plot_output_scalar.py $(IFS_SRCFILE) $(SPART_SRCFILE) $(TRIPL_SRCFILE) --reference $(MCICA_SRCFILE) --labels "SPARTACUS" "TripleClouds" --reference_label "McICA" --dstdir=$(DSTDIR)


test_output_profile:
###Plotting all profiles
	./ecrad_plot_output_profile.py $(IFS_SRCFILE) $(MCICA_SRCFILE) $(SPART_SRCFILE) $(TRIPL_SRCFILE) --latitudes $(LATITUDES) --labels "McICA" "SPARTACUS" "TripleClouds" --dstdir=$(DSTDIR)

test_output_profile_difference:
###Plotting differences between MCICA the other runs
	./ecrad_plot_output_profile.py $(IFS_SRCFILE) $(SPART_SRCFILE) $(TRIPL_SRCFILE) --reference $(MCICA_SRCFILE) --latitudes $(LATITUDES) --labels "SPARTACUS" "TripleClouds" --reference_label "McICA" --dstdir=$(DSTDIR)


test: test_inputs test_input_profile test_output test_output_scalar test_output_profile test_output_profile_difference
