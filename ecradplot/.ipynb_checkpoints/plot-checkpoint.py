"""
Filename:     plot.py
Author:       Shannon Mason, shannon.mason@ecmwf.int
Description:  Plotting functions 
"""

import pandas as pd
import numpy as np

#For loading and handling netCDF data
import xarray as xr

#Use seaborn to control matplotlib visual style
import matplotlib.pyplot as plt
import seaborn as sns

#For log-scaled colormaps
from matplotlib.colors import LogNorm

#Plot formatting functions
from ecradplot.general import *

#I/O functions
from ecradplot.io import *

#### Set plotting style ####
sns.set_style('ticks')
sns.set_context('poster')


def add_temperature_contours(ax, ds, x_dim='latitude'):
    """
    Draw contours of temperature (from ds) to ax.
    """
    
    _cn = ax.contour(ds.latitude, ds.pressure_hl.median(x_dim), ds.temperature_hl.T - 273.15,
                     levels=np.arange(-80,41,10),
                     colors=['k'], linewidths=[1.5,0.5,1.0, 0.5, 1.5, 0.5, 1.0, 0.5, 2.5, 0.5, 1.0, 0.5, 1.5, 0.5, 1.0])
    ax.clabel(_cn, [l for l in [-80,-60,-40,-20,0,20,40] if l in _cn.levels], inline=1, fmt='$%.0f^{\circ}$C', fontsize='xx-small', colors=['k'])

    
def plot_inputs_noncloud(IFS_srcfile, dstfile=None, title=None, line_ds='steps-mid'):
    """
    Plot multiple-panel figure describing non-cloud inputs to ecRAD.
    
    If aerosol mixing ratios are included in IFS_srcfile:
    - (a) solar zenith angle on left and shortwave surface albedo on right
    - (b) skin temperature on left and longwave surface emissivity on right
    - (c) 
    """
    
    _ds = load_inputs(IFS_srcfile)
    
    #Set up figure and axes
    if 'aerosol_mmr' in _ds:
        nrows=5
    else:
        nrows=4
        
    fig, axes= plt.subplots(figsize=(25,4*nrows), nrows=nrows, sharex=True)

    #First panel: SW & surface fields
    i=0
    _ds.cos_solar_zenith_angle.where(_ds.cos_solar_zenith_angle >= 0).plot(ax=axes[i], x='latitude', color='k', lw=3)
    axes[i].set_xlabel('')
    axes[i].set_ylabel(r'$\cos \theta_s$ [-]')
    axes[i].set_yticks([0,0.5,1.])
    axes[i].set_title('Solar zenith angle and shortwave albedo')
    if 'solar_irradiance' in _ds:
        axes[i].text(0.001, 1.01, f"Solar irradiance\n$Q={_ds.solar_irradiance.values:5.1f}$ W m$^{{-2}}$", ha='left', va='bottom', fontsize='small', transform=axes[i].transAxes)
    _ax0 = axes[i].twinx()
    _ax0.yaxis.set_label_position("right")
    _ax0.yaxis.tick_right()
        
    if hasattr(_ds, 'sw_albedo_band'):
        _ds.sw_albedo.isel(sw_albedo_band=2).plot.step(ax=_ax0, x='latitude', color=sns.color_palette()[0], lw=4, ds=line_ds)
    else:
        _ds.sw_albedo.plot(ax=_ax0, x='latitude', color=sns.color_palette()[0], lw=4, ds=line_ds)
    _ax0.set_yticks([0,0.5,1.0])
    _ax0.set_yticklabels([0,0.5,1.0],  color=sns.color_palette()[0])
    _ax0.set_ylabel(r'$\alpha_{SW}$ [-]', color=sns.color_palette()[0])

    #Second panel: LW surface fields
    i+=1
    _ds.skin_temperature.plot(ax=axes[i], x='latitude', color='k', lw=3)
    axes[i].set_xlabel('')
    axes[i].set_ylabel(r'$T_s$ [K]')
    axes[i].set_title('Skin temperature and longwave emissivity')

    _ax1 = axes[i].twinx()
    _ax1.yaxis.set_label_position("right")
    _ax1.yaxis.tick_right()
    if hasattr(_ds, 'lw_emissivity_band'):
        _ds.lw_emissivity.isel(lw_emissivity_band=1).plot.step(ax=_ax1, x='latitude', color=sns.color_palette()[3], lw=4, ds=line_ds)
    else:
        _ds.lw_emissivity.plot(ax=_ax1, x='latitude', color=sns.color_palette()[3], lw=4, ds=line_ds)
    _ax1.set_yticks([0.9,0.95,1.0])
    _ax1.set_ylim(0.89,1.0)
    _ax1.set_yticklabels([0.9,0.95,1.0],  color=sns.color_palette()[3])
    _ax1.set_ylabel(r'$\epsilon_{LW}$ [-]', color=sns.color_palette()[3])

    if 'aerosol_mmr' in _ds:
        i += 1
        #Third panel: aerosols
        sea_salt    = _ds.aerosol_mmr.sel(aerosol_type=slice(0,3)).sum('aerosol_type')
        desert_dust = _ds.aerosol_mmr.sel(aerosol_type=slice(3,7)).sum('aerosol_type')
        hydrophilic_org = _ds.aerosol_mmr.sel(aerosol_type=6)
        hydrophobic_org = _ds.aerosol_mmr.sel(aerosol_type=7)
        black_carbon = _ds.aerosol_mmr.sel(aerosol_type=slice(8,10)).sum('aerosol_type')
        ammonium_sulphate = _ds.aerosol_mmr.sel(aerosol_type=10)
        inactive = _ds.aerosol_mmr.sel(aerosol_type=11)

        (1e9*sea_salt).sum('p').plot(ax=axes[i], x='latitude', label='sea salt', lw=4)
        (1e9*desert_dust).sum('p').plot(ax=axes[i], x='latitude', label='dust', lw=4)
        ((1e9*hydrophilic_org).sum('p') + (1e9*hydrophilic_org).sum('p')).plot(ax=axes[i], x='latitude', label='organics', lw=4)
        (1e9*black_carbon).sum('p').plot(ax=axes[i], x='latitude', label='black carbon', lw=4)
        (1e9*ammonium_sulphate).sum('p').plot(ax=axes[i], x='latitude', label='sulphate', lw=4)

        axes[i].legend(frameon=False, fontsize='small', loc='upper left', bbox_to_anchor=(1,1))
        axes[i].set_title('Aerosol concentration')
        axes[i].set_xlabel('')
        axes[i].set_ylabel('')
        axes[i].set_yscale('log')
        axes[i].set_yticks([1e0,1e2,1e4])
        axes[i].set_ylabel('mass mixing ratio\n[$\mu$g kg$^{-1}$]')

    #Specific humidity
    i+=1
    _ds.q.plot(ax=axes[i], x='latitude', y='p', vmin=1e-6, vmax=1e-2, norm=LogNorm(), cmap='Greens', yincrease=False,
                            cbar_kwargs={'pad':0.01, 'label':'mass mixing ratio\n[kg kg$^{-1}$]', 'ticks':[1e-5,1e-4,1e-3,1e-2]}, zorder=0)
    axes[i].set_title('Specific humidity')
    axes[i].set_xlabel('')

    axes[i].set_yscale('linear')
    axes[i].set_yticks([1000e2,800e2,600e2,400e2,200e2,10e2])
    axes[i].set_ylim(1000e2,10e2)

    #Ozone
    i+=1
    _ds.o3_mmr.plot(ax=axes[i], x='latitude', y='p', vmin=1e-8, vmax=1e-5, norm=LogNorm(), cmap='Blues', yincrease=False,
                            cbar_kwargs={'pad':0.01, 'label':'mass mixing ratio\n[kg kg$^{-1}$]', 'ticks':[1e-8,1e-7,1e-6,1e-5]}, zorder=0)
    axes[i].set_title('Ozone')
    axes[i].set_xlabel('')

    axes[i].set_yscale('log')
    axes[i].set_yticks([1e5,1e4,1e3,1e2])
    axes[i].set_ylim(1e5,10)

    for ax in axes[-2:]:
        add_temperature_contours(ax, _ds)
        format_pressure(ax)

    for ax in axes[:-2]:
        snap_to_axis(ax, axes[-1])

    axes[-1].set_xlim(-90,90)
    axes[-1].set_xticks(np.arange(-90,91,15))
    format_latitude(axes[-1])
    axes[-1].set_xlabel('Latitude')
    
    add_subfigure_labels(axes)
    
    if title:
        fig.suptitle(title, x=get_figure_center(axes[0]), y=get_figure_top(fig, axes[0]), va='bottom')
    
    if dstfile:
        fig.savefig(dstfile, dpi=300, bbox_inches='tight')
    else:
        return fig, axes

    
def plot_inputs_cloud(IFS_srcfile, include_effective_radius=False, dstfile=None, title=None):
    _ds = load_inputs(IFS_srcfile)
    
    if include_effective_radius:
        nrows=5
    else:
        nrows=3
        
    fig, axes = plt.subplots(figsize=(25,4*nrows), nrows=nrows, sharex=True, sharey=True, )

    _ds.cloud_fraction.plot(ax=axes[0], x='latitude', y='p', vmin=0, vmax=1, cmap='gray_r', yincrease=False, 
                            cbar_kwargs={'pad':0.01, 'label':'fraction', 'ticks':[0, 0.2, 0.4, 0.6, 0.8, 1.0]}, zorder=0)
    axes[0].set_title('Cloud fraction')
    axes[0].set_xlabel('')

    _ds.q_ice.where(_ds.q_ice > 1e-10).plot(ax=axes[1], x='latitude', y='p', vmin=1e-8, vmax=0.5e-2, 
                                            norm=LogNorm(), cmap='Blues', yincrease=False, 
                                            cbar_kwargs={'pad':0.01, 'label':'mixing ratio\n[kg kg$^{-1}$]', 'ticks':[1e-7, 1e-5, 1e-3]}, zorder=0)
    axes[1].set_title('Cloud ice water content')
    axes[1].set_xlabel('')

    _ds.q_liquid.where(_ds.q_liquid > 1e-10).plot(ax=axes[2], x='latitude', y='p', vmin=1e-8, vmax=0.5e-2, norm=LogNorm(), 
                                                  cmap='Reds', yincrease=False, 
                                                  cbar_kwargs={'pad':0.01, 'label':'mixing ratio\n[kg kg$^{-1}$]', 'ticks':[1e-7, 1e-5, 1e-3]}, zorder=0)
    axes[2].set_title('Cloud liquid water content')
    format_latitude(axes[-1])
    
    if include_effective_radius:
        axes[2].set_xlabel('')
        
        _ds.re_ice.where(_ds.q_ice > 1e-10).plot(ax=axes[3], x='latitude', y='p', vmin=3e-6, vmax=1e-4, norm=LogNorm(), 
                                                 cmap='Blues', yincrease=False, cbar_kwargs={'pad':0.01, 'label':'$r_{\mathrm{eff}}$ [m]'}, zorder=0)
        axes[3].set_title('Ice effective radius')
        axes[3].set_xlabel('')

        _ds.re_liquid.where(_ds.q_liquid > 1e-10).plot(ax=axes[4], x='latitude', y='p', vmin=3e-6, vmax=1e-4, norm=LogNorm(), 
                                                       cmap='Reds', yincrease=False, cbar_kwargs={'pad':0.01, 'label':'$r_{\mathrm{eff}}$ [m]'}, zorder=0)
        axes[4].set_title('Liquid effective radius')

    for ax in axes:
        add_temperature_contours(ax, _ds)
        format_pressure(ax)

    axes[-1].set_xlim(-90,90)
    axes[-1].set_xticks(np.arange(-90,91,15))
    axes[0].set_ylim(1e5,3000)
    axes[-1].set_xlabel('Latitude')
        
    add_subfigure_labels(axes)
        
    if title:
        fig.suptitle(title, x=get_figure_center(axes[0]))
        
    if dstfile:
        fig.savefig(dstfile, dpi=300, bbox_inches='tight')
    else:
        return fig, axes

    
def plot_LW_flux(IFS_srcfile, ecRAD_srcfile, label=None, dstfile=None, clearsky=False):
    _ds = load_ecRAD(ecRAD_srcfile, IFS_srcfile)
    
    # LW fluxes
    nrows=3
    fig, axes = plt.subplots(figsize=(25,nrows*4), nrows=nrows, sharex=True, sharey=True)

    if clearsky:
        _ds.flux_dn_lw_clear.plot(ax=axes[0], x='latitude', y='phl', cmap='Reds', yincrease=False, vmin=0, vmax=500,
                            cbar_kwargs={'pad':0.01, 'label':'flux [W m$^{-2}$]'})
        axes[0].set_title("Clear-sky downwelling longwave flux")
    else:
        _ds.flux_dn_lw.plot(ax=axes[0], x='latitude', y='phl', cmap='Reds', yincrease=False, vmin=0, vmax=500,
                            cbar_kwargs={'pad':0.01, 'label':'flux [W m$^{-2}$]'})
        axes[0].set_title("Downwelling longwave flux")
    axes[0].set_xlabel('')

    if clearsky:
        _ds.flux_up_lw_clear.plot(ax=axes[1], x='latitude', y='phl', cmap='Reds', yincrease=False, vmin=0, vmax=500,
                        cbar_kwargs={'pad':0.01, 'label':'flux [W m$^{-2}$]'})
        axes[1].set_title("Clear-sky upwelling longwave flux")
    else:
        _ds.flux_up_lw.plot(ax=axes[1], x='latitude', y='phl', cmap='Reds', yincrease=False, vmin=0, vmax=500,
                        cbar_kwargs={'pad':0.01, 'label':'flux [W m$^{-2}$]'})
        axes[1].set_title("Upwelling longwave flux")
    axes[1].set_xlabel('')

    if clearsky:
        _ds.flux_net_lw_clear.plot(ax=axes[2], x='latitude', y='phl', cmap='RdBu_r', vmin=-500, vmax=500, 
                                           yincrease=False, cbar_kwargs={'pad':0.01, 'label':'flux [W m$^{-2}$]'})
        axes[2].set_title("Clear-sky net longwave flux")
    else:
        _ds.flux_net_lw.plot(ax=axes[2], x='latitude', y='phl', cmap='RdBu_r', vmin=-500, vmax=500, 
                                           yincrease=False, cbar_kwargs={'pad':0.01, 'label':'flux [W m$^{-2}$]'})
        axes[2].set_title("Net longwave flux")

    for ax in axes:
        add_temperature_contours(ax, _ds)
        format_pressure(ax)

    for ax in axes:
        if True:
            ax.set_yscale('linear')
            ax.set_yticks([1000e2,800e2,600e2,400e2,200e2,10e2])
            ax.set_ylim(1000e2,10e2)
        else:
            ax.set_yscale('log')
            ax.set_yticks([1e5,1e4,1e3])
            ax.set_ylim(1e5,100)
        format_pressure(ax)

    axes[-1].set_xlim(-90,90)
    axes[-1].set_xticks(np.arange(-90,91,15))
    format_latitude(axes[-1])
    axes[-1].set_xlabel('Latitude')
    
    if label:
        place_suptitle(fig, axes, label)
    
    add_subfigure_labels(axes)
    
    if dstfile:
        fig.savefig(dstfile, dpi=300, bbox_inches='tight')
    else:
        return fig, axes
    
    
def plot_LW_flux_difference(IFS_srcfile, ecRAD_srcfile, reference_ecRAD_srcfile, label=None, dstfile=None, clearsky=False):
    ds = load_ecRAD(reference_ecRAD_srcfile, IFS_srcfile)
    _ds = load_ecRAD(ecRAD_srcfile, IFS_srcfile)
    
    # LW fluxes
    nrows=3
    fig, axes = plt.subplots(figsize=(25,nrows*4), nrows=nrows, sharex=True, sharey=True)

    if clearsky:
        (_ds.flux_dn_lw_clear - ds.flux_dn_lw_clear).plot(ax=axes[0], x='latitude', y='phl', cmap='RdBu_r', yincrease=False, #vmin=0, vmax=500,
                            cbar_kwargs={'pad':0.01, 'label':'flux [W m$^{-2}$]'})
        axes[0].set_title("Clear-sky downwelling longwave flux")
    else:
        (_ds.flux_dn_lw - ds.flux_dn_lw).plot(ax=axes[0], x='latitude', y='phl', cmap='RdBu_r', yincrease=False, #vmin=0, vmax=500,
                            cbar_kwargs={'pad':0.01, 'label':'flux [W m$^{-2}$]'})
        axes[0].set_title("Downwelling longwave flux")
    axes[0].set_xlabel('')

    if clearsky:
        (_ds.flux_up_lw_clear - ds.flux_up_lw_clear).plot(ax=axes[1], x='latitude', y='phl', cmap='RdBu_r', yincrease=False, #vmin=0, vmax=500,
                        cbar_kwargs={'pad':0.01, 'label':'flux [W m$^{-2}$]'})
        axes[1].set_title("Clear-sky upwelling longwave flux")
    else:
        (_ds.flux_up_lw - ds.flux_up_lw).plot(ax=axes[1], x='latitude', y='phl', cmap='RdBu_r', yincrease=False, #vmin=0, vmax=500,
                        cbar_kwargs={'pad':0.01, 'label':'flux [W m$^{-2}$]'})
        axes[1].set_title("Upwelling longwave flux")
    axes[1].set_xlabel('')

    if clearsky:
        (_ds.flux_net_lw_clear - ds.flux_net_lw_clear).plot(ax=axes[2], x='latitude', y='phl', cmap='RdBu_r', #vmin=-500, vmax=500, 
                                           yincrease=False, cbar_kwargs={'pad':0.01, 'label':'flux [W m$^{-2}$]'})
        axes[2].set_title("Clear-sky net longwave flux")
    else:
        (_ds.flux_net_lw - ds.flux_net_lw).plot(ax=axes[2], x='latitude', y='phl', cmap='RdBu_r', #vmin=-500, vmax=500, 
                                           yincrease=False, cbar_kwargs={'pad':0.01, 'label':'flux [W m$^{-2}$]'})
        axes[2].set_title("Net longwave flux")

    for ax in axes:
        add_temperature_contours(ax, _ds)
        format_pressure(ax)

    for ax in axes:
        if True:
            ax.set_yscale('linear')
            ax.set_yticks([1000e2,800e2,600e2,400e2,200e2,10e2])
            ax.set_ylim(1000e2,10e2)
        else:
            ax.set_yscale('log')
            ax.set_yticks([1e5,1e4,1e3])
            ax.set_ylim(1e5,100)
        format_pressure(ax)

    axes[-1].set_xlim(-90,90)
    axes[-1].set_xticks(np.arange(-90,91,15))
    format_latitude(axes[-1])
    axes[-1].set_xlabel('Latitude')
    
    if label:
        place_suptitle(fig, axes, label)
    
    add_subfigure_labels(axes)
    
    if dstfile:
        fig.savefig(dstfile, dpi=300, bbox_inches='tight')
    else:
        return fig, axes
    
    
def plot_SW_flux(IFS_srcfile, ecRAD_srcfile, label=None, dstfile=None, clearsky=False):
    _ds = load_ecRAD(ecRAD_srcfile, IFS_srcfile)    
    
    # SW fluxes
    nrows=3
    fig, axes = plt.subplots(figsize=(25,nrows*4), nrows=nrows, sharex=True, sharey=True)

    if clearsky:
        _ds.flux_dn_sw_clear.plot(ax=axes[0], x='latitude', y='phl', cmap='Blues', yincrease=False, vmin=0, vmax=1200,
                        cbar_kwargs={'pad':0.01, 'label':'flux [W m$^{-2}$]'})
        axes[0].set_title("Clear-sky downwelling shortwave flux")
    else:
        _ds.flux_dn_sw.plot(ax=axes[0], x='latitude', y='phl', cmap='Blues', yincrease=False, vmin=0, vmax=1200,
                        cbar_kwargs={'pad':0.01, 'label':'flux [W m$^{-2}$]'})
        axes[0].set_title("Downwelling shortwave flux")
    axes[0].set_xlabel('')

    if clearsky:
        _ds.flux_up_sw_clear.plot(ax=axes[1], x='latitude', y='phl', cmap='Blues', yincrease=False, vmin=0, vmax=1200,
                        cbar_kwargs={'pad':0.01, 'label':'flux [W m$^{-2}$]'})
        axes[1].set_title("Clear-sky upwelling shortwave flux")
    else:
        _ds.flux_up_sw.plot(ax=axes[1], x='latitude', y='phl', cmap='Blues', yincrease=False, vmin=0, vmax=1200,
                        cbar_kwargs={'pad':0.01, 'label':'flux [W m$^{-2}$]'})
        axes[1].set_title("Upwelling shortwave flux")
    axes[1].set_xlabel('')

    if clearsky:
        _ds.flux_net_sw_clear.plot(ax=axes[2], x='latitude', y='phl', cmap='RdBu_r', vmin=-1200, vmax=1200, 
                         yincrease=False, cbar_kwargs={'pad':0.01, 'label':'flux [W m$^{-2}$]'})
        axes[2].set_title("Clear-sky net shortwave flux")
    else:
        _ds.flux_net_sw.plot(ax=axes[2], x='latitude', y='phl', cmap='RdBu_r', vmin=-1200, vmax=1200, 
                         yincrease=False, cbar_kwargs={'pad':0.01, 'label':'flux [W m$^{-2}$]'})
        axes[2].set_title("Net shortwave flux")

    for ax in axes:
        add_temperature_contours(ax, _ds) 
        format_pressure(ax)

    for ax in axes:
        if True:
            ax.set_yscale('linear')
            ax.set_yticks([1000e2,800e2,600e2,400e2,200e2,10e2])
            ax.set_ylim(1000e2,10e2)
        else:
            ax.set_yscale('log')
            ax.set_yticks([1e5,1e4,1e3,1e2,1e1])
            ax.set_ylim(1e5,10)
        format_pressure(ax)

    axes[-1].set_xlim(-90,90)
    axes[-1].set_xticks(np.arange(-90,91,15))
    format_latitude(axes[-1])
    axes[-1].set_xlabel('Latitude')
               
    if label:
        place_suptitle(fig, axes, label)

    add_subfigure_labels(axes)
        
    if dstfile:
        fig.savefig(dstfile, dpi=300, bbox_inches='tight')
    else:
        return fig, axes
    
    
def plot_SW_flux_difference(IFS_srcfile, ecRAD_srcfile, reference_ecRAD_srcfile, label=None, dstfile=None, clearsky=False):
    ds = load_ecRAD(reference_ecRAD_srcfile, IFS_srcfile)
    _ds = load_ecRAD(ecRAD_srcfile, IFS_srcfile)
    
    # SW fluxes
    nrows=3
    fig, axes = plt.subplots(figsize=(25,nrows*4), nrows=nrows, sharex=True, sharey=True)

    if clearsky:
        (_ds.flux_dn_sw_clear - ds.flux_dn_sw_clear).plot(ax=axes[0], x='latitude', y='phl', cmap='RdBu_r', yincrease=False, #vmin=0, vmax=1200,
                        cbar_kwargs={'pad':0.01, 'label':'flux [W m$^{-2}$]'})
        axes[0].set_title("Clear-sky downwelling shortwave flux")
    else:
        (_ds.flux_dn_sw - ds.flux_dn_sw).plot(ax=axes[0], x='latitude', y='phl', cmap='RdBu_r', yincrease=False, #vmin=0, vmax=1200,
                        cbar_kwargs={'pad':0.01, 'label':'flux [W m$^{-2}$]'})
        axes[0].set_title("Downwelling shortwave flux")
    axes[0].set_xlabel('')

    if clearsky:
        (_ds.flux_up_sw_clear - ds.flux_up_sw_clear).plot(ax=axes[1], x='latitude', y='phl', cmap='RdBu_r', yincrease=False, #vmin=0, vmax=1200,
                        cbar_kwargs={'pad':0.01, 'label':'flux [W m$^{-2}$]'})
        axes[1].set_title("Clear-sky upwelling shortwave flux")
    else:
        (_ds.flux_up_sw - ds.flux_up_sw).plot(ax=axes[1], x='latitude', y='phl', cmap='RdBu_r', yincrease=False, #vmin=0, vmax=1200,
                        cbar_kwargs={'pad':0.01, 'label':'flux [W m$^{-2}$]'})
        axes[1].set_title("Upwelling shortwave flux")
    axes[1].set_xlabel('')

    if clearsky:
        (_ds.flux_net_sw_clear - ds.flux_net_sw_clear).plot(ax=axes[2], x='latitude', y='phl', cmap='RdBu_r', #vmin=-1200, vmax=1200, 
                         yincrease=False, cbar_kwargs={'pad':0.01, 'label':'flux [W m$^{-2}$]'})
        axes[2].set_title("Clear-sky net shortwave flux")
    else:
        (_ds.flux_net_sw - ds.flux_net_sw).plot(ax=axes[2], x='latitude', y='phl', cmap='RdBu_r', #vmin=-1200, vmax=1200, 
                         yincrease=False, cbar_kwargs={'pad':0.01, 'label':'flux [W m$^{-2}$]'})
        axes[2].set_title("Net shortwave flux")

    for ax in axes:
        add_temperature_contours(ax, _ds) 
        format_pressure(ax)

    for ax in axes:
        if True:
            ax.set_yscale('linear')
            ax.set_yticks([1000e2,800e2,600e2,400e2,200e2,10e2])
            ax.set_ylim(1000e2,10e2)
        else:
            ax.set_yscale('log')
            ax.set_yticks([1e5,1e4,1e3,1e2,1e1])
            ax.set_ylim(1e5,10)
        format_pressure(ax)

    axes[-1].set_xlim(-90,90)
    axes[-1].set_xticks(np.arange(-90,91,15))
    format_latitude(axes[-1])
    axes[-1].set_xlabel('Latitude')
               
    if label:
        place_suptitle(fig, axes, label)

    add_subfigure_labels(axes)
        
    if dstfile:
        fig.savefig(dstfile, dpi=300, bbox_inches='tight')
    else:
        return fig, axes
    

def plot_CRE(IFS_srcfile, ecRAD_srcfile, label=None, dstfile=None):
    _ds = load_ecRAD(ecRAD_srcfile, IFS_srcfile)    
    
    nrows=3
    fig, axes = plt.subplots(figsize=(25,nrows*4), nrows=nrows, sharex=True, sharey=True)

    _ds.cloud_radiative_effect_sw.plot(ax=axes[0], x='latitude', y='phl', cmap='RdBu_r', yincrease=False, vmin=-500, vmax=500,
                cbar_kwargs={'pad':0.01, 'label':'CRE$_{\mathrm{SW}}$ [W m$^{-2}$]'})
    axes[0].set_xlabel('')
    axes[0].set_title("Shortwave cloud radiative effect")

    _ds.cloud_radiative_effect_lw.plot(ax=axes[1], x='latitude', y='phl', cmap='RdBu_r', yincrease=False, vmin=-200, vmax=200,
                        cbar_kwargs={'pad':0.01, 'label':'CRE$_{\mathrm{SW}}$ [W m$^{-2}$]'})
    axes[1].set_xlabel('')
    axes[1].set_title("Longwave cloud radiative effect")

    (_ds.cloud_radiative_effect_sw + _ds.cloud_radiative_effect_lw).plot(ax=axes[2], x='latitude', y='phl', cmap='RdBu_r', vmin=-500, vmax=500, 
                         yincrease=False, cbar_kwargs={'pad':0.01, 'label':'CRE$_\mathrm{net}$ [W m$^{-2}$]'})
    axes[2].set_title("Net cloud radiative effect")

    for ax in axes:
        add_temperature_contours(ax, _ds) 
        format_pressure(ax)

    for ax in axes:
        if True:
            ax.set_yscale('linear')
            ax.set_yticks([1000e2,800e2,600e2,400e2,200e2,10e2])
            ax.set_ylim(1000e2,10e2)
        else:
            ax.set_yscale('log')
            ax.set_yticks([1e5,1e4,1e3,1e2,1e1])
            ax.set_ylim(1e5,10)
        format_pressure(ax)

    axes[-1].set_xlim(-90,90)
    axes[-1].set_xticks(np.arange(-90,91,15))
    format_latitude(axes[-1])
    axes[-1].set_xlabel('Latitude')
               
    if label:
        place_suptitle(fig, axes, label)

    add_subfigure_labels(axes)
        
    if dstfile:
        fig.savefig(dstfile, dpi=300, bbox_inches='tight')
    else:
        return fig, axes
    
def plot_CRE_difference(IFS_srcfile, ecRAD_srcfile, reference_ecRAD_srcfile, label=None, dstfile=None):
    ds = load_ecRAD(reference_ecRAD_srcfile, IFS_srcfile)    
    _ds = load_ecRAD(ecRAD_srcfile, IFS_srcfile)    
    
    nrows=3
    fig, axes = plt.subplots(figsize=(25,nrows*4), nrows=nrows, sharex=True, sharey=True)

    (_ds.cloud_radiative_effect_sw - ds.cloud_radiative_effect_sw).plot(ax=axes[0], x='latitude', y='phl', cmap='RdBu_r', yincrease=False, #vmin=-500, vmax=500,
                cbar_kwargs={'pad':0.01, 'label':'CRE$_{\mathrm{SW}}$ [W m$^{-2}$]'})
    axes[0].set_xlabel('')
    axes[0].set_title("Shortwave cloud radiative effect")

    (_ds.cloud_radiative_effect_lw - ds.cloud_radiative_effect_lw).plot(ax=axes[1], x='latitude', y='phl', cmap='RdBu_r', yincrease=False, #vmin=-200, vmax=200,
                        cbar_kwargs={'pad':0.01, 'label':'CRE$_{\mathrm{SW}}$ [W m$^{-2}$]'})
    axes[1].set_xlabel('')
    axes[1].set_title("Longwave cloud radiative effect")

    ((_ds.cloud_radiative_effect_sw + _ds.cloud_radiative_effect_lw) - (ds.cloud_radiative_effect_sw + ds.cloud_radiative_effect_lw)).plot(ax=axes[2], x='latitude', y='phl', 
                                                                                                                                           cmap='RdBu_r', #vmin=-500, vmax=500, 
                         yincrease=False, cbar_kwargs={'pad':0.01, 'label':'CRE$_\mathrm{net}$ [W m$^{-2}$]'})
    axes[2].set_title("Net cloud radiative effect")

    for ax in axes:
        add_temperature_contours(ax, _ds) 
        format_pressure(ax)

    for ax in axes:
        if True:
            ax.set_yscale('linear')
            ax.set_yticks([1000e2,800e2,600e2,400e2,200e2,10e2])
            ax.set_ylim(1000e2,10e2)
        else:
            ax.set_yscale('log')
            ax.set_yticks([1e5,1e4,1e3,1e2,1e1])
            ax.set_ylim(1e5,10)
        format_pressure(ax)

    axes[-1].set_xlim(-90,90)
    axes[-1].set_xticks(np.arange(-90,91,15))
    format_latitude(axes[-1])
    axes[-1].set_xlabel('Latitude')
               
    if label:
        place_suptitle(fig, axes, label)

    add_subfigure_labels(axes)
        
    if dstfile:
        fig.savefig(dstfile, dpi=300, bbox_inches='tight')
    else:
        return fig, axes

    
def plot_heating_rate(IFS_srcfile, ecRAD_srcfile, label=None, linear_pressure=True, dstfile=None):
    _ds = load_ecRAD(ecRAD_srcfile, IFS_srcfile)

    if linear_pressure:
        vmax = 10
    else:
        vmax = 30
    
    nrows=3
    fig, axes = plt.subplots(figsize=(25,nrows*4), nrows=nrows, sharex=True, sharey=True)
    
    _ds.heating_rate_lw.plot(ax=axes[0], x='latitude', y='p', cmap='RdBu_r', yincrease=False, vmin=-vmax, vmax=vmax,
                        cbar_kwargs={'pad':0.01, 'label':'heating rate\n[K d$^{-1}$]'})
    axes[0].set_xlabel('')
    axes[0].set_title("Longwave heating rate")


    _ds.heating_rate_sw.plot(ax=axes[1], x='latitude', y='p', cmap='RdBu_r', yincrease=False, vmin=-vmax, vmax=vmax,
                        cbar_kwargs={'pad':0.01, 'label':'heating rate\n[K d$^{-1}$]'})
    axes[1].set_xlabel('')
    axes[1].set_title("Shortwave heating rate")

    (_ds.heating_rate_sw + _ds.heating_rate_lw).plot(ax=axes[2], x='latitude', y='p', cmap='RdBu_r', vmin=-vmax, vmax=vmax,
                         yincrease=False, cbar_kwargs={'pad':0.01, 'label':'heating rate\n[K d$^{-1}$]'})
    axes[2].set_title("Net heating rate")

    for ax in axes:
        add_temperature_contours(ax, _ds)
        format_pressure(ax)

    for ax in axes:
        if linear_pressure:
            ax.set_yscale('linear')
            ax.set_yticks([1000e2,800e2,600e2,400e2,200e2,10e2])
            ax.set_ylim(1000e2,10e2)
        else:
            ax.set_yscale('log')
            ax.set_yticks([1e5,1e4,1e3,1e2,1e1])
            ax.set_ylim(1e5,10)
        format_pressure(ax)

    axes[-1].set_xlim(-90,90)
    axes[-1].set_xticks(np.arange(-90,91,15))
    format_latitude(axes[-1])
    axes[-1].set_xlabel('Latitude')
    
    if label:
        place_suptitle(fig, axes, label)

    add_subfigure_labels(axes)
    
    if dstfile:
        fig.savefig(dstfile, dpi=300, bbox_inches='tight')
    else:
        return fig, axes
    
    
def plot_heating_rate_difference(IFS_srcfile, ecRAD_srcfile, reference_ecRAD_srcfile, label=None, linear_pressure=True, dstfile=None):
    ds = load_ecRAD(reference_ecRAD_srcfile, IFS_srcfile)
    _ds = load_ecRAD(ecRAD_srcfile, IFS_srcfile)

    if linear_pressure:
        vmax = 10
    else:
        vmax = 30
    
    nrows=3
    fig, axes = plt.subplots(figsize=(25,nrows*4), nrows=nrows, sharex=True, sharey=True)
    
    (_ds.heating_rate_lw - ds.heating_rate_lw).plot(ax=axes[0], x='latitude', y='p', cmap='RdBu_r', yincrease=False, vmin=-vmax, vmax=vmax,
                        cbar_kwargs={'pad':0.01, 'label':'heating rate\n[K d$^{-1}$]'})
    axes[0].set_xlabel('')
    axes[0].set_title("Longwave heating rate")


    (_ds.heating_rate_sw - ds.heating_rate_sw).plot(ax=axes[1], x='latitude', y='p', cmap='RdBu_r', yincrease=False, vmin=-vmax, vmax=vmax,
                        cbar_kwargs={'pad':0.01, 'label':'heating rate\n[K d$^{-1}$]'})
    axes[1].set_xlabel('')
    axes[1].set_title("Shortwave heating rate")

    
    ((_ds.heating_rate_sw + _ds.heating_rate_lw) - (ds.heating_rate_sw + ds.heating_rate_lw)).plot(ax=axes[2], x='latitude', y='p', cmap='RdBu_r', vmin=-vmax, vmax=vmax,
                         yincrease=False, cbar_kwargs={'pad':0.01, 'label':'heating rate\n[K d$^{-1}$]'})
    axes[2].set_title("Net heating rate")

    for ax in axes:
        add_temperature_contours(ax, _ds)
        format_pressure(ax)

    for ax in axes:
        if linear_pressure:
            ax.set_yscale('linear')
            ax.set_yticks([1000e2,800e2,600e2,400e2,200e2,10e2])
            ax.set_ylim(1000e2,10e2)
        else:
            ax.set_yscale('log')
            ax.set_yticks([1e5,1e4,1e3,1e2,1e1])
            ax.set_ylim(1e5,10)
        format_pressure(ax)

    axes[-1].set_xlim(-90,90)
    axes[-1].set_xticks(np.arange(-90,91,15))
    format_latitude(axes[-1])
    axes[-1].set_xlabel('Latitude')
    
    if label:
        place_suptitle(fig, axes, label)

    add_subfigure_labels(axes)
    
    if dstfile:
        fig.savefig(dstfile, dpi=300, bbox_inches='tight')
    else:
        return fig, axes
    

def plot_output_scalar(IFS_srcfile, ecRAD_srcfiles, ecRAD_styles, dstfile=None, latitudes_to_highlight=None, line_ds='default'):

    nrows=4
    fig, axes = plt.subplots(figsize=(25,4*nrows), nrows=nrows, sharex=True)

    for _style, _srcfile in zip(ecRAD_styles, ecRAD_srcfiles):
        _ds = load_ecRAD(_srcfile, IFS_srcfile)

        _ds.flux_dn_sw.isel(phl=-1).plot(ax=axes[0], x='latitude', ds=line_ds, **_style)
        _ds.cloud_radiative_effect_sw.isel(phl=0).plot(ax=axes[1], x='latitude', ds=line_ds, **_style)
        _ds.cloud_radiative_effect_lw.isel(phl=0).plot(ax=axes[2], x='latitude', ds=line_ds, **_style)
        _ds.cloud_cover_sw.plot(ax=axes[3], x='latitude', ds=line_ds, **_style)

    axes[0].set_ylabel('flux [W m$^{-2}$]')
    axes[0].set_title('Shortwave downwelling surface flux')
    axes[0].set_xlabel('')
    axes[0].legend(loc='upper left', bbox_to_anchor=(1,1), fontsize='small', frameon=False)

    axes[1].set_ylabel('CRE [W m$^{-2}$]')
    axes[1].set_title("Shortwave TOA cloud radiative effect")
    axes[1].set_xlabel('')

    axes[2].set_ylabel('CRE [W m$^{-2}$]')
    axes[2].set_title("Longwave TOA cloud radiative effect")
    axes[2].set_xlabel('')

    axes[3].set_ylabel('CF [-]')
    axes[3].set_title("Cloud cover")
    axes[3].set_ylim(-0.1, 1.1)
    axes[3].set_yticks([0, 0.5, 1])
    axes[3].set_xlim(-84,84)
    format_latitude(axes[3])
    axes[3].set_xlabel('Latitude')

    #Highlight latitudes of interest
    if latitudes_to_highlight:
        for ax in axes:
            for i, _lat in enumerate(latitudes_to_highlight):
                ax.axvline(_lat, lw=10, alpha=0.5, color=sns.color_palette()[i])

    add_subfigure_labels(axes)
    
    axes[-1].set_xlim(-90,90)
    axes[-1].set_xticks(np.arange(-90,91,15))
    format_latitude(axes[-1])
    axes[-1].set_xlabel('Latitude')
    
    if dstfile:
        fig.savefig(dstfile, dpi=300, bbox_inches='tight')
    else:
        return fig, axes
    
    
def plot_output_scalar_difference(IFS_srcfile, ecRAD_srcfiles, reference_ecRAD_srcfile, ecRAD_styles, reference_label="", line_ds='default', dstfile=None):

    nrows=4
    fig, axes = plt.subplots(figsize=(25,4*nrows), nrows=nrows, sharex=True)

    ds = load_ecRAD(reference_ecRAD_srcfile, IFS_srcfile)
    
    for _style, _srcfile in zip(ecRAD_styles, ecRAD_srcfiles):
        _ds = load_ecRAD(_srcfile, IFS_srcfile)

        (_ds.flux_dn_sw - ds.flux_dn_sw).isel(phl=-1).plot(ax=axes[0], x='latitude', ds=line_ds, **_style)
        (_ds.cloud_radiative_effect_sw - ds.cloud_radiative_effect_sw).isel(phl=0).plot(ax=axes[1], x='latitude', ds=line_ds, **_style)
        (_ds.cloud_radiative_effect_lw - ds.cloud_radiative_effect_lw).isel(phl=0).plot(ax=axes[2], x='latitude', ds=line_ds, **_style)
        (_ds.cloud_cover_sw - ds.cloud_cover_sw).plot(ax=axes[3], x='latitude', ds=line_ds, **_style)

    axes[0].set_ylabel('flux [W m$^{-2}$]')
    axes[0].set_title('Shortwave downwelling surface flux')
    axes[0].set_xlabel('')
    axes[0].legend(loc='upper left', bbox_to_anchor=(1,1), fontsize='small', frameon=False)

    axes[1].set_ylabel('CRE [W m$^{-2}$]')
    axes[1].set_title("Shortwave TOA cloud radiative effect")
    axes[1].set_xlabel('')

    axes[2].set_ylabel('CRE [W m$^{-2}$]')
    axes[2].set_title("Longwave TOA cloud radiative effect")
    axes[2].set_xlabel('')

    axes[3].set_ylabel('cloud cover [-]')
    axes[3].set_title("Cloud cover")
    axes[3].set_xlim(-90,90)
    format_latitude(axes[3])
    axes[3].set_xlabel('Latitude')

    if False:
        #Highlight latitudes of interest
        for ax in axes:
            ax.axvline(-58, lw=10, alpha=0.5, color=sns.color_palette()[0])
            ax.axvline(-8, lw=10, alpha=0.5, color=sns.color_palette()[1])
            ax.axvline(3,   lw=10, alpha=0.5, color=sns.color_palette()[2])
            ax.axvline(36,  lw=10, alpha=0.5, color=sns.color_palette()[3])

    add_subfigure_labels(axes)
    
    if reference_label:
        fig.suptitle(f"Difference vs {reference_label}")
    
    if dstfile:
        fig.savefig(dstfile, dpi=300, bbox_inches='tight')
    else:
        return fig, axes
    
    
    
def plot_on_hybrid_pressure_axis(_axes, x, y, linedict):
    #Log part of the plot
    _axes[0].plot(x, y, **linedict)
    _axes[0].set_yscale('log')
    format_pressure(_axes[0], label='')
    _axes[0].set_yticks([1000,100,10,1])
    _axes[0].set_ylim(10000,1)
    
    #Linear part of the plot
    _axes[1].plot(x, y, **linedict)
    format_pressure(_axes[1], label='')
    _axes[1].set_yticks(np.linspace(9e4,1e4,5, dtype='float'))
    _axes[1].set_ylim(100000,10000)
    
    # Hide the right and top spines
    _axes[0].spines['bottom'].set_visible(False)
    _axes[1].spines['top'].set_visible(False)
    _axes[0].spines['bottom'].set_visible(False)
    _axes[1].spines['top'].set_visible(False)
    
    _axes[0].axhline(10000, lw=5, color='0.67', ls='-', zorder=-11)
    _axes[0].text(1., 0, 'log', color='0.67', fontsize='small', va='bottom', ha='right', transform=_axes[0].transAxes, zorder=-10)
    _axes[1].text(1., 1, 'linear', color='0.67', fontsize='small', va='top', ha='right', transform=_axes[1].transAxes, zorder=-10)
    
    
def label_hybrid_pressure_axes(_axes):
    l = _axes[0].set_ylabel('Pressure [hPa]')
    x, y = l.get_position()
    l.set_position((x, y - 1)) 
    
    
def plot_input_profile(lat, IFS_srcfile, dstfile=None):

    from ecradplot import io as eio
    from ecradplot import plot as eplt
    
    ncols=7
    nrows=2
    fig, axes = plt.subplots(figsize=(4*ncols,8), ncols=ncols, nrows=nrows, sharex='col', sharey='row', gridspec_kw={'hspace':0, 'height_ratios':[1,2]})
 
    _ds = eio.load_inputs(IFS_srcfile).sel(latitude=lat, method='nearest')

    #Temperature
    i=0
    plot_on_hybrid_pressure_axis(axes[:,i], _ds.temperature_hl, _ds.pressure_hl, {'lw':5})
    #axes[i].plot(_ds.temperature_hl, _ds.pressure_hl, **{'lw':5, 'color':sns.color_palette()[3]})
    
    #Specific humidity
    i+=1
    plot_on_hybrid_pressure_axis(axes[:,i], _ds.q, _ds.pressure_fl, {'lw':5})
        
    #Cloud fraction
    i+=1
    plot_on_hybrid_pressure_axis(axes[:,i], _ds.cloud_fraction, _ds.pressure_fl, {'lw':5, 'color':'0.5'})

    #Water content
    i+=1
    plot_on_hybrid_pressure_axis(axes[:,i], 1e6*_ds.q_liquid.where(_ds.q_liquid > 1e-10).fillna(0), _ds.pressure_fl, 
                                 {'lw':5, 'color':sns.color_palette()[3], 'label':'liquid'})
    plot_on_hybrid_pressure_axis(axes[:,i], 1e6*_ds.q_ice.where(_ds.q_ice > 1e-10).fillna(0), _ds.pressure_fl, 
                                 {'lw':5, 'color':sns.color_palette()[0], 'label':'ice'})
        
    #Ozone
    i+=1
    plot_on_hybrid_pressure_axis(axes[:,i], _ds.o3_mmr, _ds.pressure_fl, {'label':'O$_3$', 'lw':5, 'color':sns.color_palette()[1]})
    
    #O2 + C02 + CH4 + N20 + CFC
    i+=1
    plot_on_hybrid_pressure_axis(axes[:,i], _ds.ch4_vmr, _ds.pressure_fl, {'label':'CH$_4$', 'lw':5, 'color':sns.color_palette()[0]})
    plot_on_hybrid_pressure_axis(axes[:,i], _ds.n2o_vmr, _ds.pressure_fl, {'label':'N$_2$O', 'lw':5, 'color':sns.color_palette()[3]})
        
    i+=1 
    plot_on_hybrid_pressure_axis(axes[:,i], 1e6*_ds.co2_vmr, _ds.pressure_fl, {'label':'CO$_2$', 'lw':5, 'color':sns.color_palette()[2]})
    
    #Aerosols
    #There are no aerosols
    
    axes[0,0].set_title('Temperature')
    axes[1,0].set_xlabel("$T$ [K]")
    axes[1,0].set_xlim(170,320)
    
    axes[0,1].set_title('Specific\nhumidity')
    axes[1,1].set_xlabel("$q$ [kg kg$^{-1}$]")
    axes[1,1].set_xlim(1e-6,1e-1)
    axes[1,1].set_xscale('log')
        
    axes[0,2].set_title('Cloud fraction')
    axes[1,2].set_xlabel('[-]')
    axes[1,2].set_xlim(0,1)
    
    axes[0,3].set_title('Cloud water content')
    axes[1,3].set_xlabel('$q$ [kg kg$^{-1}$]')
        
    axes[0,4].set_title('Ozone')
    axes[1,4].set_xlabel("$q_i$ [kg kg$^{-1}$]")
    axes[1,4].set_xscale('log')
    
    axes[0,5].set_title('Other gases')
    axes[1,5].set_xlabel('$q_i$ [ppm]')
    axes[1,5].set_xscale('log')
    
    axes[0,6].set_title('Other gases')
    axes[1,6].set_xlabel('$q_i$ [ppm]')
   
    axes[0,3].legend(frameon=False, fontsize='small')
    axes[0,-2].legend(frameon=False, loc='upper right', fontsize='small')
    axes[0,-1].legend(frameon=False, loc='upper right', fontsize='small')
        
    fig.suptitle(f'Atmospheric profile at {fancy_format_latitude(lat)}', y=1.05)
    
    label_hybrid_pressure_axes(axes[:,0])
    
    if dstfile:
        fig.savefig(dstfile, dpi=300, bbox_inches='tight')
    else:
        return fig, axes



def plot_rad_profile(IFS_srcfile, ecRAD_srcfiles, latitude, linedicts, dstfile=None):

    ncols=8
    fig, axes = plt.subplots(figsize=(4*ncols,8), nrows=2, ncols=ncols, sharex='col', sharey='row', gridspec_kw={'hspace':0, 'height_ratios':[1,2]})

    for j, ecRAD_srcfile in enumerate(ecRAD_srcfiles):
        _ds = load_ecRAD(ecRAD_srcfile, IFS_srcfile).sel(latitude=latitude, method='nearest')  

        i = 0
        plot_on_hybrid_pressure_axis(axes[:,i], _ds.flux_dn_lw, _ds.pressure_hl, linedicts[j])
        plot_on_hybrid_pressure_axis(axes[:,i], _ds.flux_up_lw, _ds.pressure_hl, {**linedicts[j], **{'alpha':0.0}})

        i+=1
        plot_on_hybrid_pressure_axis(axes[:,i], _ds.flux_up_lw, _ds.pressure_hl, linedicts[j])
        plot_on_hybrid_pressure_axis(axes[:,i], _ds.flux_dn_lw, _ds.pressure_hl, {**linedicts[j], **{'alpha':0.0}})

        i+=1
        plot_on_hybrid_pressure_axis(axes[:,i], _ds.flux_dn_sw, _ds.pressure_hl, linedicts[j])
        plot_on_hybrid_pressure_axis(axes[:,i], _ds.flux_up_sw, _ds.pressure_hl, {**linedicts[j], **{'alpha':0.0}})

        i+=1
        plot_on_hybrid_pressure_axis(axes[:,i], _ds.flux_up_sw, _ds.pressure_hl, linedicts[j])
        plot_on_hybrid_pressure_axis(axes[:,i], _ds.flux_dn_sw, _ds.pressure_hl, {**linedicts[j], **{'alpha':0.0}})

        i+=1
        plot_on_hybrid_pressure_axis(axes[:,i], _ds.cloud_radiative_effect_lw, _ds.pressure_hl, linedicts[j])

        i+=1
        plot_on_hybrid_pressure_axis(axes[:,i], _ds.cloud_radiative_effect_sw, _ds.pressure_hl, linedicts[j])

        i+=1
        plot_on_hybrid_pressure_axis(axes[:,i], _ds.heating_rate_lw, _ds.pressure_fl, linedicts[j])
    
        i+=1
        plot_on_hybrid_pressure_axis(axes[:,i], _ds.heating_rate_sw, _ds.pressure_fl, linedicts[j])

    heating_rate_min = np.floor(np.min(axes[0,-2].get_xlim() + axes[1,-2].get_xlim() + axes[0,-1].get_xlim() + axes[1,-1].get_xlim()))
    heating_rate_max = np.ceil(np.max(axes[0,-2].get_xlim() + axes[1,-2].get_xlim() + axes[0,-1].get_xlim() + axes[1,-1].get_xlim()))
    heating_rate_abs_lim = np.max([np.abs(heating_rate_min), heating_rate_max])
        
    axes[1,-2].set_xlim(-1*heating_rate_abs_lim, None)
    axes[1,-1].set_xlim(None, heating_rate_abs_lim)
        
    axes[0,0].set_title('Downwelling\nlongwave flux', color=sns.color_palette()[3])
    axes[0,1].set_title('Upwelling\nlongwave flux', color=sns.color_palette()[3])
    axes[0,2].set_title('Downwelling\nshortwave flux', color=sns.color_palette()[0])
    axes[0,3].set_title('Upwelling\nshortwave flux', color=sns.color_palette()[0])
    axes[0,4].set_title('Longwave\nCRE', color=sns.color_palette()[3])
    axes[0,5].set_title('Shortwave\nCRE', color=sns.color_palette()[0])
    axes[0,6].set_title('Longwave\nheating rate', color=sns.color_palette()[3])
    axes[0,7].set_title('Shortwave\nheating rate', color=sns.color_palette()[0])
    
    axes[1,0].set_xlabel('[W m$^{-2}$]')
    axes[1,1].set_xlabel('[W m$^{-2}$]')
    axes[1,2].set_xlabel('[W m$^{-2}$]')
    axes[1,3].set_xlabel('[W m$^{-2}$]')
    axes[1,4].set_xlabel('[W m$^{-2}$]')
    axes[1,5].set_xlabel('[W m$^{-2}$]')
    axes[1,6].set_xlabel('[K d$^{-1}$]')
    axes[1,7].set_xlabel('[K d$^{-1}$]')
    
    axes[0,-1].legend(loc='upper left', frameon=False, bbox_to_anchor=(1,1))

    label_hybrid_pressure_axes(axes[:,0])
    
    fig.suptitle(f'Atmospheric profile at {fancy_format_latitude(latitude)}', y=1.05)
        
    if dstfile:
        fig.savefig(dstfile, dpi=300, bbox_inches='tight')
    else:
        return fig, axes
    
    
def plot_rad_profile_difference(IFS_srcfile, ecRAD_reference_srcfile, ecRAD_srcfiles, latitude, linedicts, reference_label, dstfile=None):

    ncols=8
    fig, axes = plt.subplots(figsize=(4*ncols,8), nrows=2, ncols=ncols, sharex='col', sharey='row', gridspec_kw={'hspace':0, 'height_ratios':[1,2]})

    ds = load_ecRAD(ecRAD_reference_srcfile, IFS_srcfile).sel(latitude=latitude, method='nearest')  
    
    for j, ecRAD_srcfile in enumerate(ecRAD_srcfiles):
        _ds = load_ecRAD(ecRAD_srcfile, IFS_srcfile).sel(latitude=latitude, method='nearest')  

        i = 0
        plot_on_hybrid_pressure_axis(axes[:,i], _ds.flux_dn_lw - ds.flux_dn_lw, _ds.pressure_hl, linedicts[j])

        i+=1
        plot_on_hybrid_pressure_axis(axes[:,i], _ds.flux_up_lw - ds.flux_up_lw, _ds.pressure_hl, linedicts[j])

        i+=1
        plot_on_hybrid_pressure_axis(axes[:,i], _ds.flux_dn_sw - ds.flux_dn_sw, _ds.pressure_hl, linedicts[j])

        i+=1
        plot_on_hybrid_pressure_axis(axes[:,i], _ds.flux_up_sw - ds.flux_up_sw, _ds.pressure_hl, linedicts[j])

        i+=1
        plot_on_hybrid_pressure_axis(axes[:,i], _ds.cloud_radiative_effect_lw - ds.cloud_radiative_effect_lw, _ds.pressure_hl, linedicts[j])

        i+=1
        plot_on_hybrid_pressure_axis(axes[:,i], _ds.cloud_radiative_effect_sw - ds.cloud_radiative_effect_sw, _ds.pressure_hl, linedicts[j])

        i+=1
        plot_on_hybrid_pressure_axis(axes[:,i], _ds.heating_rate_lw - ds.heating_rate_lw, _ds.pressure_fl, linedicts[j])
    
        i+=1
        plot_on_hybrid_pressure_axis(axes[:,i], _ds.heating_rate_sw - ds.heating_rate_sw, _ds.pressure_fl, linedicts[j])

    
    axes[0,0].set_title('Downwelling\nlongwave flux', color=sns.color_palette()[3])
    axes[0,1].set_title('Upwelling\nlongwave flux', color=sns.color_palette()[3])
    axes[0,2].set_title('Downwelling\nshortwave flux', color=sns.color_palette()[0])
    axes[0,3].set_title('Upwelling\nshortwave flux', color=sns.color_palette()[0])
    axes[0,4].set_title('Longwave\nCRE', color=sns.color_palette()[3])
    axes[0,5].set_title('Shortwave\nCRE', color=sns.color_palette()[0])
    axes[0,6].set_title('Longwave\nheating rate', color=sns.color_palette()[3])
    axes[0,7].set_title('Shortwave\nheating rate', color=sns.color_palette()[0])
    
    axes[1,0].set_xlabel('[W m$^{-2}$]')
    axes[1,1].set_xlabel('[W m$^{-2}$]')
    axes[1,2].set_xlabel('[W m$^{-2}$]')
    axes[1,3].set_xlabel('[W m$^{-2}$]')
    axes[1,4].set_xlabel('[W m$^{-2}$]')
    axes[1,5].set_xlabel('[W m$^{-2}$]')
    axes[1,6].set_xlabel('[K d$^{-1}$]')
    axes[1,7].set_xlabel('[K d$^{-1}$]')
    
    legend = axes[0,-1].legend(loc='upper left', frameon=False, bbox_to_anchor=(1,1))
    legend.set_title(f'Difference vs\n{reference_label}')

    label_hybrid_pressure_axes(axes[:,0])

    for ax in axes[0,:]:
        ax.axhline(10000, lw=5, color='0.67', ls='-', zorder=-11)
        ax.text(1., 0, 'log', color='0.67', fontsize='small', va='bottom', ha='right', transform=ax.transAxes, zorder=-10)
        
    for ax in axes[1,:]:
        ax.text(1., 1, 'linear', color='0.67', fontsize='small', va='top', ha='right', transform=ax.transAxes, zorder=-10)

    fig.suptitle(f'Atmospheric profile at {fancy_format_latitude(latitude)}', y=1.05)
        
    if dstfile:
        fig.savefig(dstfile, dpi=300, bbox_inches='tight')
    else:
        return fig, axes