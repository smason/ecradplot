#!/usr/bin/env python3

import numpy as np
from ecradplot import plot as eplt

def main(input_srcfile, output_srcfiles, reference_output, output_labels, reference_label, latitudes, dstdir, title):
    """
    Plot input files
    """
    
    import os
    import seaborn as sns
    name_string  = os.path.splitext(os.path.basename(input_srcfile))[0]

    styles = [{'lw':5,   'color':'k', 'ls':'-'},
              {'lw':5,   'color':'0.67', 'ls':'-'},
              {'lw':5,   'color':'0.33', 'ls':'-'},
              {'lw':2,   'color':sns.color_palette()[0], 'ls':'--'},
              {'lw':2,   'color':sns.color_palette()[2], 'ls':'--'},
              {'lw':2,   'color':sns.color_palette()[3], 'ls':'--'},
              {'lw':3.5, 'color':sns.color_palette()[5], 'ls':'-.'},
              {'lw':3.5, 'color':sns.color_palette()[6], 'ls':'-.'},
              {'lw':3.5, 'color':sns.color_palette()[9], 'ls':'-.'}]
    
    n = len(output_srcfiles)
    styles = [{**styles[i], **{'label':output_labels[i]}} for i, output_srcfile in enumerate(output_srcfiles)]
       
    format_latitude = lambda x: r"{:.0f}S".format(-1*x) if x < 0 else "{:.0f}N".format(x)

    if reference_output:
        for _lat in latitudes:
            eplt.plot_rad_difference_profile(input_srcfile, reference_output, output_srcfiles, _lat, styles, reference_label,
                              dstfile=f"{dstdir}/{name_string}_profile_comparison_{format_latitude(_lat)}.png")
    else:
        for _lat in latitudes:
            eplt.plot_rad_profile(input_srcfile, output_srcfiles, _lat, styles, 
                              dstfile=f"{dstdir}/{name_string}_profile_comparison_{format_latitude(_lat)}.png")
    
if __name__ == "__main__":
    import argparse
    parser = argparse.ArgumentParser(description="Plot radiative fluxes and heating rates from ecRAD output file. \
                                     If a reference file is given, plot differences with respect to the reference.")
    parser.add_argument("input",     help="ecRAD input file")
    parser.add_argument("outputs",   help="ecRAD output files", nargs='+')
    parser.add_argument("--reference", help="ecRAD output file to use as a reference", default=None)
    parser.add_argument("--latitudes", help="Latitudes at which to extract profiles", nargs='+', type=np.float)
    parser.add_argument("--labels",  help="Labels for ecRAD output files", nargs='*')
    parser.add_argument("--reference_label", help="Label for ecRAD output file used as a reference", default=None)
    parser.add_argument("--dstdir",  help="Destination directory for plots", default="./")
    parser.add_argument("--title",   help="Overwrite figure title", default=None)
    args = parser.parse_args()
    
    main(args.input, args.outputs, args.reference, args.labels, args.reference_label, args.latitudes, args.dstdir, args.title)
