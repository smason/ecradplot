#!/usr/bin/env python3 

def warn(*args, **kwargs):
    pass
    
import os, warnings
warnings.warn = warn
#warnings.simplefilter(action = "ignore", category = RuntimeWarning)    

#import yaml
#yaml.warnings({'YAMLLoadWarning': False})
#os.environ['PYTHONWARNINGS']="ignore::yaml.YAMLLoadWarning"

from ecradplot import plot as eplt

def main(input_srcfile, dstdir, title):
    """
    Plot input files
    """
         
    
    if not os.path.isdir(dstdir):
        os.makedirs(dstdir)

    name_string      = os.path.splitext(os.path.basename(input_srcfile))[0]
    dstfile_noncloud = os.path.join(dstdir, name_string + "_inputs_surface_and_atmospheric_composition.png")
    dstfile_cloud    = os.path.join(dstdir, name_string + "_inputs_cloud.png")
    dstfile_aerosol  = os.path.join(dstdir, name_string + "_inputs_aerosol.png")
    
    if not title:
        title = name_string 
        
    print(f"Plotting surface and atmospheric composition inputs to {dstfile_noncloud}")
    eplt.plot_inputs_noncloud(input_srcfile, dstfile=dstfile_noncloud, title=title + "\nsurface and atmospheric composition", line_ds='default');
    
    print(f"Plotting cloud inputs to {dstfile_cloud}")
    eplt.plot_inputs_cloud(input_srcfile, dstfile=dstfile_cloud, title=title + "\nclouds");
    
    print(f"Plotting aerosol inputs to {dstfile_aerosol}")
    eplt.plot_inputs_aerosols(input_srcfile, dstfile=dstfile_aerosol, title=title + "\naerosols");
    
if __name__ == "__main__":
    import argparse
    parser = argparse.ArgumentParser(description="Plot surface properties, atmospheric composition and clouds from input file to ecRAD.")
    parser.add_argument("input",    help="ecRAD input file")
    parser.add_argument("--dstdir", help="Destination directory for plots", default="./")
    parser.add_argument("--title",  help="Figure title", default=None)
    args = parser.parse_args()
    
    main(args.input, args.dstdir, args.title)
