#!/usr/bin/env python3

def warn(*args, **kwargs):
    pass
    
import os, warnings
warnings.warn = warn

from ecradplot import plot as eplt

def main(input_srcfile, latitudes, dstdir, title):
    """
    Plot input files
    """
    
    import os
    if not os.path.isdir(dstdir):
        os.makedirs(dstdir)

    name_string      = os.path.splitext(os.path.basename(input_srcfile))[0]
    
    if not title:
        title = name_string 
    
    for _lat in latitudes:
        dstfile = os.path.join(dstdir, name_string + f"_input_profiles_{eplt.unfancy_format_latitude(_lat)}.png")
        
        print(f"Plotting inputs profile to {dstfile}")
        eplt.plot_input_profile(_lat, input_srcfile, dstfile=dstfile, title=title);
    
if __name__ == "__main__":
    import argparse
    parser = argparse.ArgumentParser(description="Plot profiles of atmospheric composition and clouds from input file to ecRAD.")
    parser.add_argument("input",  help="ecRAD input file")
    parser.add_argument("--latitudes", help="Latitudes at which to extract profiles", nargs='+', type=float)
    parser.add_argument("--dstdir", help="Destination directory for plots", default="./")
    parser.add_argument("--title",  help="Figure title", default=None)
    args = parser.parse_args()
    
    main(args.input, args.latitudes, args.dstdir, args.title)
