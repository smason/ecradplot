#!/usr/bin/env python3

from ecradplot import plot as eplt

def main(input_srcfile, output_srcfiles, output_labels, dstdir, title):
    """
    Plot input files
    """
    
    import os
    import seaborn as sns
    name_string  = os.path.splitext(os.path.basename(input_srcfile))[0]
    
    styles = [{'lw':5, 'color':sns.color_palette()[0], 'ls':'-'},
              {'lw':3, 'color':sns.color_palette()[2], 'ls':'--'},
              {'lw':2, 'color':sns.color_palette()[3], 'ls':':'},
              {'lw':4, 'color':'0.67', 'ls':'-', 'zorder':-3},
              {'lw':3, 'color':'0.67', 'ls':'--', 'zorder':-2},
              {'lw':4, 'color':'0.33', 'ls':'-', 'zorder':-1},
              {'lw':3, 'color':'0.33', 'ls':'--', 'zorder':-1}]
        
    n = len(output_srcfiles)
    styles = [{**styles[i], **{'label':output_labels[i]}} for i, output_srcfile in enumerate(output_srcfiles)]
       
    eplt.plot_comparison_surface_and_TOA_cloud_and_radiation(input_srcfile, output_srcfiles, styles, dstfile=f"{dstdir}/{name_string}_zonal_comparison.png");
    
if __name__ == "__main__":
    import argparse
    parser = argparse.ArgumentParser(description="Plot radiative fluxes and heating rates from ecRAD output file.")
    parser.add_argument("input",    help="ecRAD input file")
    parser.add_argument("outputs",  help="ecRAD output files", nargs='+')
    parser.add_argument("--labels", help="Labels for ecRAD output files", nargs='*')
    parser.add_argument("--dstdir", help="Destination directory for plots", default="./")
    parser.add_argument("--title",  help="Overwrite figure title", default=None)
    args = parser.parse_args()
    
    print(f"{len(args.outputs)} output files given")
    print(f"{len(args.labels)} output labels given")
    
    main(args.input, args.outputs, args.labels, args.dstdir, args.title)
