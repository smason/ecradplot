#!/usr/bin/env python3

def warn(*args, **kwargs):
    pass
    
import os, warnings
warnings.warn = warn

from ecradplot import plot as eplt

def main(input_srcfile, output_srcfiles, output_labels, reference_srcfile, reference_label, dstdir, title):
    """
    Plot input files
    """
    
    import os
    if not os.path.isdir(dstdir):
        os.makedirs(dstdir)

    import seaborn as sns
    name_string  = os.path.splitext(os.path.basename(input_srcfile))[0]
        
    if not title:
        title = name_string
        
    styles = [{'lw':5,   'color':'k', 'ls':'-'},
              {'lw':3.5,   'color':'0.33', 'ls':'--'},
              {'lw':2,   'color':'0.67', 'ls':'-.'},
              {'lw':3.5,   'color':sns.color_palette()[0], 'ls':'--'},
              {'lw':3.5,   'color':sns.color_palette()[2], 'ls':'--'},
              {'lw':3.5,   'color':sns.color_palette()[3], 'ls':'--'},
              {'lw':2, 'color':sns.color_palette()[5], 'ls':'-.'},
              {'lw':2, 'color':sns.color_palette()[6], 'ls':'-.'},
              {'lw':2, 'color':sns.color_palette()[9], 'ls':'-.'}]
        
    n = len(output_srcfiles)
    styles = [{**styles[i], **{'label':output_labels[i]}} for i, output_srcfile in enumerate(output_srcfiles)]

    if reference_srcfile:
        dstfile = f"{dstdir}/{name_string}_surface_and_TOA_vs_{reference_label}.png"
        print(f"Plotting integrated and TOA outputs to {dstfile}")
        eplt.plot_output_scalar_difference(input_srcfile, output_srcfiles, reference_srcfile, styles, reference_label, title=title,
                                           dstfile=dstfile)
    else:
        dstfile = f"{dstdir}/{name_string}_surface_and_TOA.png"
        print(f"Plotting integrated and TOA outputs to {dstfile}")
        eplt.plot_output_scalar(input_srcfile, output_srcfiles, styles, title=title, dstfile=dstfile)
    
if __name__ == "__main__":
    import argparse
    parser = argparse.ArgumentParser(description="Plot radiative fluxes and heating rates from ecRAD output file.")
    parser.add_argument("input",    help="ecRAD input file")
    parser.add_argument("outputs",  help="ecRAD output files", nargs='+')
    parser.add_argument("--reference", help="ecRAD output file to use as a reference", default=None)
    parser.add_argument("--labels", help="Labels for ecRAD output files", nargs='*')
    parser.add_argument("--reference_label", help="Label for ecRAD output file used as a reference", default=None)
    parser.add_argument("--dstdir", help="Destination directory for plots", default="./")
    parser.add_argument("--title",  help="Overwrite figure title", default=None)
    args = parser.parse_args()
        
    main(args.input, args.outputs, args.labels, args.reference, args.reference_label, args.dstdir, args.title)
